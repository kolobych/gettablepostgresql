<%@ page import="com.kolobych.web.entity.Table" %>
<%@ page import="com.kolobych.web.entity.Row" %>
<%@ page import="java.util.ArrayList" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Andriy.Kolobych
  Date: 30.09.2016
  Time: 12:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Web Application</title>
  </head>
  <body>
    <form action="${pageContext.request.contextPath}/TestServlet" method="POST">
      <input type="submit" value="Get Data" />
    </form>
    <table border="1">
      <tr>
        <%--@elvariable id="tableColumns" type="java.util.List"--%>
        <c:forEach items="${tableColumns}" var="tblColName">
          <td><c:out value="${tblColName}"/></td>
        </c:forEach>
      </tr>
      <%--@elvariable id="tableRows" type="com.kolobych.web.entity.Row"--%>
      <c:forEach items="${tableRows}" var="row">
        <tr>
          <c:forEach items="${row.values}" var="value">
            <td><c:out value="${value}"/></td>
          </c:forEach>
        </tr>
      </c:forEach>
    </table>
  </body>
</html>
