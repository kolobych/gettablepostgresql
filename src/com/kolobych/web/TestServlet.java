package com.kolobych.web;
import com.kolobych.web.entity.Table;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Andriy.Kolobych on 30.09.2016.
 */
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Context env;
        Table table = null;
        try {
            env = (Context) new InitialContext().lookup("java:comp/env");
            String dbUrl = (String)env.lookup("dbUrl");
            String dbLogin = (String)env.lookup("dbLogin");
            String dbPass = (String)env.lookup("dbPass");
            String tableName = (String)env.lookup("tableName");
            table = DAO.getDAO(dbUrl.trim(), dbLogin.trim(), dbPass.trim()).getTable(tableName.trim());
        } catch (NamingException e) {
            e.printStackTrace();
        }

        req.setAttribute("tableColumns", table.getNamesOfColumns());
        req.setAttribute("tableRows", table.getRows());

        req.getRequestDispatcher("index.jsp").forward(req,resp);
    }


}
