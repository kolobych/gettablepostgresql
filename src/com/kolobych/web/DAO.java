package com.kolobych.web;

import com.kolobych.web.entity.Row;
import com.kolobych.web.entity.Table;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andriy.Kolobych on 30.09.2016.
 */
public class DAO {
    private static DAO DAO;
    private final String dbURL;
    private final String dbUser;
    private final String dbPassword;

    private DAO(final String dbURL, final String dbUser, final String dbPassword) {
        this.dbURL = dbURL;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        try {
            Context env = (Context) new InitialContext().lookup("java:comp/env");
            String dbDriver = (String) env.lookup("dbDriver");
            Class.forName(dbDriver);
        } catch (NamingException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DAO getDAO(final String dbURL, final String dbUser, final String dbPassword) {
        if (DAO == null) DAO = new DAO(dbURL, dbUser, dbPassword);
        return DAO;
    }

    public Table getTable(String tableName) {

        List<String> columnNames = getColumnNames(tableName);
        Table table = new Table();
        table.setNamesOfColumns(columnNames);
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = getConnection();
        try {
            st = conn.prepareStatement(String.format("SELECT * FROM %s", tableName));
            rs = st.executeQuery();
            while (rs.next()) {
                List<String> rowValues = new ArrayList<>();
                for (String columnName : columnNames) {
                    try {
                        rowValues.add(new String(rs.getBytes(columnName), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                table.addRow(new Row(rowValues));
            }
        } catch (SQLException e) {
            System.err.printf("> Error while getting table values. Details: %s%n", e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                assert st != null;
                st.close();
                assert rs != null;
                rs.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return table;
    }

    private Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbURL, dbUser, dbPassword);
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
        return conn;
    }

    private List<String> getColumnNames(String tableName) {
        List<String> columnsNames = new ArrayList<>();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = getConnection();
        try {
            st = conn.prepareStatement(String.format("SELECT * FROM %s", tableName));
            rs = st.executeQuery();
            for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                columnsNames.add(rs.getMetaData().getColumnName(i + 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                assert rs != null;
                rs.close();
                st.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return columnsNames;
    }
}
