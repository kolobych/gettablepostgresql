package com.kolobych.web.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andriy.Kolobych on 30.09.2016.
 */
public class Table {
    private List<String> namesOfColumns;
    private List<Row> rows;

    public Table() {
        this.namesOfColumns = new ArrayList<>();
        this.rows = new ArrayList<>();
    }

    public List<String> getNamesOfColumns() {
        return namesOfColumns;
    }

    public void setNamesOfColumns(List<String> namesOfColumns) {
        this.namesOfColumns = namesOfColumns;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public void addRow(Row row) {
        rows.add(row);
    }
}
