package com.kolobych.web.entity;

import java.util.List;

/**
 * Created by Andriy.Kolobych on 30.09.2016.
 */
public class Row {
    private List<String> values;

    public Row(List<String> values) {
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
